package com.ss.fashion.maincategeries.recycleview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ss.fashion.maincategeries.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laptop88.vn on 27/10/2016.
 */

public class AdapterRecycleViewSale extends RecyclerView.Adapter<AdapterRecycleViewSale.RecycleVIewHolder>{
    private List<Integer> imageDatas = new ArrayList<>();
    private Context context;
    public AdapterRecycleViewSale(List<Integer> imageDatas, Context context) {
        this.imageDatas = imageDatas;
        this.context = context;
    }

    @Override
    public RecycleVIewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemview = inflater.inflate(R.layout.item_recycleview_sale, parent, false);
        return new RecycleVIewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(RecycleVIewHolder holder, int position) {
        holder.ivSale.setBackgroundResource(imageDatas.get(position));
    }

    @Override
    public int getItemCount() {
        return imageDatas.size();
    }

    public class RecycleVIewHolder extends RecyclerView.ViewHolder{
        ImageView ivSale;
        public RecycleVIewHolder(View itemView) {
            super(itemView);
            ivSale = (ImageView) itemView.findViewById(R.id.ivSale);
        }
    }
}
